function ResFolder = cellulose_mc_series(run_mode_choice, param_bact, param_cell, param_par, param_graph)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% IBM simulation of the degradation of a cellulose bead (2D) 
% by a colony of bacteria (main function)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% syntax:   cellulose(run_mode_choice)
%
%     run_mode_choice
%     |     
%     |     simulation (main computation)
%     |     |  static graphic
%     |     |  |  animated graphic
%     |     |  |  |  building graphics and animation
%     |     |  |  |  |  verbose (tty output)
%     |     |  |  |  |  |  non verbose (redirected in a file)
%     |     |  |  |  |  |  |  saved files
%     |     |  |  |  |  |  |  |
%     V     V  V  V  V  V  V  V
%     ---------------------------------------------------------------------
%     0     x  x  x  -  x  -  -  BASIC MODE (with output [not saved])
%     1     x  -  -  -  -  x  x  BATCH MODE (simulation only/no output)
%     2     -  -  -  x  x  -  -  POST GRAPHICS with interface
%
%     3     x  -  -  -  -  x  x  MC BATCH MODE (simulation only/no output)
%     4     -  -  -  x  x  -  -  MC POST GRAPHICS with interface
%
%     otherwise this structure
%
%     MC : Monte Carlo trials
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% CONTENTS
%
%   main functions ........................................................
%
%     function cellulose()             interface function
%     function simulation()            main function
%     function simulation_mc()         main function for MC
%     function initialisation()        initialisation
%     function substrate_diffusion()   diffusion of the substrate
%     function bacteria_displacement() displacement of the bacteria
%     function bacteria_consumption()  consumption of the bacteria    
%     function bacteria_duplication()  duplication of the bacteria
%
%  graphics functions .....................................................
%
%     function graphics_interface()    interface for all graphic outputs
%                                      (video included)
%     function graphics_interface_mc() interface for all graphic outputs
%                                      for MC
%     function snapshot()              plot a snapshot of the system state
%     function plot_cellulose_concentration_and_bacteria()
%                plot cellulose concentration and bacetria at time g.t_out(g.i_out)
%     function plot_cellulose_concentration()
%                plot cellulose concentration at g.t_out(g.i_out)
%     function plot_total_cellulose()
%                plot t -> total mass of cellulose for t = g.t_out(1:g.i_out)
%     function plot_pop_size_and_biomass()
%                plot t -> size of the population and total biomass 
%                for t = g.t_out(1:g.i_out)
%     function make_the_movie()
%     function f_pause_continue(chain)    
%
%==========================================================================
%
% --- algorithm structure ------------------------------------------------
%
%   initialisation()   % all initialisation
%   p.t = 0;
%   for k=1:p.n_ite
%        substrate_diffusion()   % diffusion of the substrate
%        bacteria_displacement() % displacement of the bacteria
%        bacteria_consumption()  % bacteria consumption
%        bacteria_duplication()  % bacteria duplication
%        p.t = p.t+p.dt;
%   end
%
% --- time structure ------------------------------------------------------
%
%            
%   |---------|---------|---------|---------|---------|---------|
%   0                    ---------                             tmax
%                            dt
%   X            X            X            X            X       X
%
%      dt       IBM time step
%      X        instant of snaphots (graphic outputs) 
%      dt_FD    maximum finite difference time step (substrate diffusion)
%               [we use an explicit FD scheme]
%
% --- space structure -----------------------------------------------------
%
%   domaine [-L,L]^2
%   initial cellulose concentration of the centered circle of radius R
%   size of the grid n in each direction (total n^2)
%
% --- main state variables ------------------------------------------------
%
%   at a given time 
%        b.mass(i)     	                mass of the bacteria i
%        b.pos(i,1:2)                   position of the bacteria i
%   on a grid (p.n x p.n)
%        c.concentration(1:p.n,1:p.n)	cellulose concentration
%
% --- main ouput variables ------------------------------------------------
%
%    g.t_out              instants of saved data
%
%  at each instants in g.t_out, we save :
%
%    c.concentration_out  cellulose concentration
%    c.total_out          aggretate cellulose concentration
%    b.mass_out           mass of the bacteria
%    b.pos_out            position of the bacteria
%    b.sizepop_out        size of the bacterial population
%    b.mass_total_out     aggretate biomass of the bacterial population
%
% --- main MC ouput variables ---------------------------------------------
%
%    c.total_out_mc       aggretate cellulose concentration
%    b.sizepop_out_mc     size of the bacterial population
%    b.mass_total_out_mc  aggretate biomass of the bacterial population
%
%==========================================================================
% Fabien Campillo INRIA 
% Ariane Bize     CEMAGREF
% Chlo� Deygout   post-doc INRIA
% -------------------------------------------------------------------------
% http://www-sop.inria.fr/members/Fabien.Campillo/index.html
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Versions
%       2011, March 28      - version 1
%       2011, April 26      - version 2
%       2011, April 27      - version 3
%       2011, November 16   - version 4 : improved interface
%       2011, December 28   - version 5 : Monte Carlo features
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


global c b p g

% c : cellulose  data structure
% b : bacteria   data structure
% p : parameters data structure
% g : graphics   data structure


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% --- input parameters ----------------------------------------------------
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if nargin == 5
  disp('hello0')
  p = param_par;
  c = param_cell;
  b = param_bact;
  g = param_graph;

else
% --- geometry ------------------------------------------------------------

  p.n = 250;   % number of case in each direction (total n^2)
  p.L = 40;    % 1/2 length of the domain domain=[-L,L]^2                (�m)
  p.R = 25;    % radius of the subtrat circle                            (�m)

% --- time ----------------------------------------------------------------

  p.tmax = 70; % final time                                              (h)
  p.dt   = 0.3; % IBM time step (= EDP time step)                         (h)

% --- cellulose -----------------------------------------------------------

  c.ini  = 0.4;  % initial concentration in the substrate circle    (pg/�m^2)
  c.min  = 0.3;  % diffusion threshold of substrate concentration   (pg/�m^2)
  c.diff = 5;    % diffusion parameter                               (�m^2/h)

% --- bacteria ------------------------------------------------------------

  b.size_ini     = 5;   % initial bacterial population size
  b.mass_ini     = 0.4; % initial mass of 1 bacteria                     (pg)
  b.mass_dup     = 0.8; % mass of duplication                            (pg)

% --- bacteria displacement

  b.speed_max    = 10;  % max speed of the bacteria                    (�m/h)
  b.mov_drift    = 0.1; % drift coefficient                     (�m^4/(pg h))
  b.mov_diff_std = 1;   % sqrt(diffusion) coefficient            (�m/sqrt(h))

  b.cons_yield   = 0.2; % absorption percentage                           (-)
  b.cons_rate    = 0.2; % consumption rate                             (pg/h)

% --- consumption kernel (consumption of the bacteria)

  b.ker_cons_std = 1; % standard deviation                               (�m)

% --- perception kernel (displacement of the bacteria)

  b.ker_perc_std = 2; % standard deviation                               (�m)

% --- type of initialization
%        'solo'              1 bacteria in the center
%        'uniform_border'    bacterias uniformly on the circle
%        'uniform'           bacterias uniformly outside the circle

  p.ini_type = 'uniform';

% --- graphics ------------------------------------------------------------

  g.nb_out            = 500; % number of time snapshots
  g.movie             = 0;   % do a movie or not
  g.movie_window_size = 500; % size of the movie window

% --- seed ----------------------------------------------------------------

  p.iseed = 9999;

% --- mc ------------------------------------------------------------------

  p.mc = 1;   % number of MC trails 
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% --- run mode ------------------------------------------------------------
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


if nargin == 0
    run_mode_choice = 0;   % default
end

p.run_mode  = run_mode_choice;
switch  p.run_mode
    
    
    case 0 % === default mode (basic) =====================================
              
        p.fileID = 1 ; % std output
        simulation();
        
    case 1 % === batch mode ===============================================
        
        p.work_dir = ['essai_' datestr(now,30)];
        if ispc % dos
            dos(['mkdir ' p.work_dir]);
            dos(['copy cellulose_mc_series.m ' p.work_dir]);
        elseif ismac || isunix % unix
            unix(['mkdir ' p.work_dir]);
            unix(['cp cellulose_mc_series.m ' p.work_dir]);
        else % unknow os
            error(' ERROR unknow system !!!');
        end        
        p.fileID = fopen([p.work_dir '/out.txt'],'w');
        simulation();
        fclose(p.fileID);
        save([p.work_dir '/data.mat']);
        
    case 2 % === graphics interface =======================================
        
        p.fileID = 1 ; % std output
        
        fprintf(p.fileID,[' \n \n'...
            '----------------------------------------------------------------- \n' ...
            ' graphics interface                                               \n' ...
            '----------------------------------------------------------------- \n\n']);
        liste_essais = dir('essai_*');
        nb_essais    = size(liste_essais,1);
        
        fprintf(p.fileID,'      > list of tests :\n');
        for i=1:nb_essais
            fprintf(p.fileID,' \t %i  %s \n',i,liste_essais(i).name);
            %                disp(['   ' int2str(i) '  ' liste_essais(i).name])
        end
        answer=str2double(input('        choice: ','s'));
        p.work_dir =  [pwd '/' liste_essais(answer).name];
        fprintf('\n      > loading the data ... ');
        warning('off','MATLAB:loadobj')
        load([p.work_dir '/data.mat'])
        fprintf('done \n');
        p.work_dir =  [pwd '/' liste_essais(answer).name]; % modified by load
        graphics_interface()
        
    case 3 % === MC batch mode ============================================
        p.work_dir = ['resmc_' datestr(now,30)];
        if ispc % dos
            dos(['mkdir ' p.work_dir]);
            dos(['copy cellulose_mc_series.m ' p.work_dir]);
        elseif ismac || isunix % unix
            unix(['mkdir ' p.work_dir]);
            unix(['cp cellulose_mc_series.m ' p.work_dir]);
        else % unknow os
            error(' ERROR unknow system !!!');
        end        
        p.fileID = fopen([p.work_dir '/out.txt'],'w');
        simulation_mc();
        fclose(p.fileID);
        save([p.work_dir '/data.mat']);
        
    case 4 % === MC graphics interface ====================================
        
        p.fileID = 1 ; % std output
        
        fprintf(p.fileID,[' \n \n'...
            '----------------------------------------------------------------- \n' ...
            ' graphics interface (MC version)                                  \n' ...
            '----------------------------------------------------------------- \n\n']);
        liste_essais = dir('resmc_*');
        nb_essais    = size(liste_essais,1);
        
        fprintf(p.fileID,'      > list of tests :\n');
        for i=1:nb_essais
            fprintf(p.fileID,' \t %i  %s \n',i,liste_essais(i).name);
            %                disp(['   ' int2str(i) '  ' liste_essais(i).name])
        end
        answer=str2double(input('        choice: ','s'));
        p.work_dir =  [pwd '/' liste_essais(answer).name];
        fprintf('\n      > loading the data ... ');
        warning('off','MATLAB:loadobj')
        load([p.work_dir '/data.mat'])
        fprintf('done \n');
        p.work_dir =  [pwd '/' liste_essais(answer).name]; % modified by load
        graphics_interface_mc()

    otherwise % ===========================================================
        
        disp('                                                                               ')
        disp('    Syntax                                                                     ')
        disp('         cellulose(run_mode)                                                   ')
        disp('                                                                               ')
        disp('         run_mode                                                              ')
        disp('         |     simulation (main computation)                                   ')
        disp('         |     |  static graphic                                               ')
        disp('         |     |  |  animated graphic                                          ')
        disp('         |     |  |  |  building graphics and animation                        ')
        disp('         |     |  |  |  |  verbose (tty output)                                ')
        disp('         |     |  |  |  |  |  non verbose (redirected in a file)               ')
        disp('         |     |  |  |  |  |  |  saved files                                   ')
        disp('         |     |  |  |  |  |  |  |                                             ')
        disp('         V     V  V  V  V  V  V  V                                             ')
        disp('         ------------------------------------------------------------------    ')
        disp('         0     x  x  x  -  x  -  -  BASIC MODE (with output [not saved])       ')
        disp('         1     x  -  -  -  -  x  x  BATCH MODE (simulation only / no output)   ')
        disp('         2     -  -  -  x  x  -  -  POST GRAPHICS with interface               ')
        disp('                                                                               ')
        disp('         3     x  -  -  -  -  x  x  MC BATCH MODE (simulation only / no output)')
        disp('         4     -  -  -  x  x  -  -  MC POST GRAPHICS with interface            ')
        disp('         otherwise this message                                                ')
        disp('                                                                               ')
        disp('         MC : Monte Carlo trials                                               ')
        disp('                                                                               ')
        

end
ResFolder = p.work_dir;
end
%end
%OutputFold = p.work_dir % end of cellulose_mc_series()
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



function simulation()
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% simulation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

global c b p g
fprintf(p.fileID,[' \n \n'...
    '----------------------------------------------------------------- \n' ...
    ' IBM simulation of the degradation of a cellulose bead (2D) by a  \n' ...
    ' colony of bacteria                                               \n' ...
    '----------------------------------------------------------------- \n\n'...
    '   %s \n\n'],date);
clf

% --- initialisation ------------------------------------------------------

fprintf(p.fileID,'      > initialisation .... ');
seeding_the_generators()  

                                         % initialisation and 
                                         % memory allocation of :
initialisation_parameters()              %    the parameters
initialisation_substrate_concentration() %    the substrate concentration 
initialisation_bacterial_population()    %    the bacterial population

fprintf(p.fileID,'done \n');

% --- time iterations -----------------------------------------------------

g.t_last_out        = -1000000;
g.i_out             = 0;                              % index of the current i_out

tic
g.cputime_old = cputime; % for the ellapsed

fprintf(p.fileID,'      > iterations .... \n');

p.t = 0;
for k=1:p.n_ite
        
    if p.t - g.t_last_out > g.d_out
        %pause
        save_data()           % saving the data
        fprintf(p.fileID,' \t i_print %i iteration %i \n ',g.i_out,k);
        if p.run_mode == 0
            snapshot()            % graphics
        end
    end
        
    substrate_diffusion()   % diffusion of the substrate
    bacteria_displacement() % displacement of the bacteria
    bacteria_consumption()  % bacteria consumption
    bacteria_duplication()  % bacteria duplication
    
    p.t = p.t+p.dt;

end

save_data()        % saving the data 
g.i_out_last = g.i_out; 
fprintf(p.fileID,' \t i_print %i iteration %i \n ',g.i_out,k);

fprintf(p.fileID,'                       .... done \n');
if p.run_mode == 0
    snapshot()            % graphics
end
cpu_time = toc;
fprintf(p.fileID,'\n\n      > total CPU time %d (s) \n',cpu_time);

end % of simulation()
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



function simulation_mc()
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% simulation : Monte Carlo trials
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

global c b p g

fprintf(p.fileID,[' \n \n'...
    '----------------------------------------------------------------- \n' ...
    ' IBM simulation of the degradation of a cellulose bead (2D) by a  \n' ...
    ' colony of bacteria                                               \n' ...
    ' MC Monte Carlo trials                                            \n' ...
    '----------------------------------------------------------------- \n\n'...
    '   %s \n\n'],date);
%%% Printing the parameter values
fprintf(p.fileID,' \n p.n = %d',p.n);
fprintf(p.fileID,' \n p.L = %6.2f',p.L);
fprintf(p.fileID,' \n p.R = %6.2f',p.R);
fprintf(p.fileID,' \n p.tmax = %6.2f',p.tmax);
fprintf(p.fileID,' \n p.dt = %6.2f',p.dt);
fprintf(p.fileID,' \n c.ini = %6.2f',c.ini);
fprintf(p.fileID,' \n c.min = %6.2f',c.min);
fprintf(p.fileID,' \n c.diff = %6.2f',c.diff);
fprintf(p.fileID,' \n b.size_ini = %d',b.size_ini);
fprintf(p.fileID,' \n b.mass_ini = %6.2f',b.mass_ini);
fprintf(p.fileID,' \n b.mass_dup = %6.2f',b.mass_dup);
fprintf(p.fileID,' \n b.speed_max = %6.2f',b.speed_max);
fprintf(p.fileID,' \n b.mov_drift = %6.2f',b.mov_drift);
fprintf(p.fileID,' \n b.mov_diff_std = %6.2f',b.mov_diff_std);
fprintf(p.fileID,' \n b.cons_yield = %6.2f',b.cons_yield);
fprintf(p.fileID,' \n b.cons_rate = %6.2f',b.cons_rate);
fprintf(p.fileID,' \n b.ker_cons_std = %6.2f',b.ker_cons_std);
fprintf(p.fileID,' \n b.ker_perc_std = %6.2f',b.ker_perc_std);
fprintf(p.fileID,' \n p.ini_type = %s',p.ini_type);
fprintf(p.fileID,' \n g.nb_out = %d',g.nb_out);
fprintf(p.fileID,' \n g.movie = %d',g.movie);
fprintf(p.fileID,' \n g.movie_window_size = %6.2f',g.movie_window_size);
fprintf(p.fileID,' \n p.iseed = %6.2f',p.iseed);
fprintf(p.fileID,' \n p.mc = %d\n\n',p.mc);

clf

% --- seeding the generators ----------------------------------------------

seeding_the_generators()  
                                 
% --- initialisation of the parameters/memory allocation ------------------

initialisation_parameters() % ini   


% --- MC iterations -------------------------------------------------------

for i_mc=1:p.mc
    
    g.t_last_out        = -1000000;
    g.i_out             = 0;          % index of the current i_out

    tic
    g.cputime_old = cputime; % for the ellapsed
    
    fprintf(p.fileID,'      > MC iteration %i \n ',i_mc);
    
    fprintf(p.fileID,'      > initialisation .... ');
                                             % initialisation and 
                                             % memory allocation of :
    initialisation_substrate_concentration() %    the substrate concentration
    initialisation_bacterial_population()    %    the bacterial population
    fprintf(p.fileID,'done \n');
    
    % --- time iterations
        
    fprintf(p.fileID,'      > time iterations .... \n');
    p.t = 0;
    for k=1:p.n_ite
        
        if p.t - g.t_last_out > g.d_out
            save_data()           % saving the data
            fprintf(p.fileID,' \t mc %i i_print %i iteration %i \n ',i_mc,g.i_out,k);
        end
        
        substrate_diffusion()   % diffusion of the substrate
        bacteria_displacement() % displacement of the bacteria
        bacteria_consumption()  % bacteria consumption
        bacteria_duplication()  % bacteria duplication
        
        p.t = p.t+p.dt;
        
    end
    
    save_data()        % saving the data
    g.i_out_last = g.i_out;
    fprintf(p.fileID,' \t mc %i i_print %i iteration %i \n ',i_mc,g.i_out,k);


    c.total_out_mc(i_mc,:)      = c.total_out;
    b.sizepop_out_mc(i_mc,:)    = b.sizepop_out;
    b.mass_total_out_mc(i_mc,:) = b.mass_total_out; 

end

end % of simulation_mc() 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% subfunctions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



function seeding_the_generators()
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

global c b p g

% --- setting the random stream and seeding it (seulement pour R2010)

% mtstream = RandStream('mt19937ar','Seed',iseed);
% RandStream.setDefaultStream(mtstream);

% --- version plus ancienne

randn('state',p.iseed)
rand('state',p.iseed)

end % seeding_the_generators()
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



function initialisation_parameters()
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% initialisation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

global c b p g

%--------------------------------------------------------------------------
% technical parameters (derived from input parameters)
%--------------------------------------------------------------------------

% --- geometry ------------------------------------------------------------

p.h   = 2 * p.L / p.n; % step size
p.h2  = p.h * p.h;     % step size ^2
p.hh2 = 1 / p.h2;      % 1 / step size ^2
p.hc2 = 1 / (2 * p.h);

% --- grid ----------------------------------------------------------------

[p.gx,p.gy] = meshgrid( -p.L+p.h/2 : p.h : p.L-p.h/2 , ...
    -p.L+p.h/2 : p.h : p.L-p.h/2); 

% --- time ----------------------------------------------------------------

p.sqrt_dt = sqrt(p.dt);
p.dt_FD   = 0.25 * p.h2 / c.diff;              % max step for DF scheme
p.n_ite   = floor(p.tmax/p.dt);                % number of time iteration
g.d_out   = floor((p.tmax/p.dt)/(g.nb_out-1)); % number of time iterations 
                                               % between two outputs

% --- consumption kernel of the bacteria ----------------------------------
% (see bacteria_consumption)

b.ker_cons_c = -1/(2*b.ker_cons_std*b.ker_cons_std);

% --- bacteria perception (perception kernel) for the displacement --------

b.ker_perc_c = -1/(2*b.ker_perc_std*b.ker_perc_std);
b.ker_perc   = exp(b.ker_perc_c * (p.gx.*p.gx+p.gy.*p.gy));

%--- initialisation/memory allocation of outputs --------------------------

g.t_out             = zeros(1,g.nb_out+1);            % instants of saved data

% --- at each instants in g.t_out, we save :

c.concentration_out = zeros(p.n,p.n,g.nb_out+1);      % cellulose concentration
c.total_out         = zeros(1,g.nb_out+1);            % aggretate cellulose concentration

b.max_size = 1000;

b.mass_out          = zeros(b.max_size,1,g.nb_out+1); % mass of the bacteria
b.pos_out           = zeros(b.max_size,2,g.nb_out+1); % position of the bacteria
b.sizepop_out       = zeros(1,g.nb_out+1);            % size of the bacterial population
b.mass_total_out    = zeros(1,g.nb_out+1);            % aggretate biomass of the bacterial population

% --- idem in the MC context ----------------------------------------------

c.total_out_mc      = zeros(p.mc,g.nb_out+1);        
b.sizepop_out_mc    = zeros(p.mc,g.nb_out+1);
b.mass_total_out_mc = zeros(p.mc,g.nb_out+1); 

end % of initialisation_parameters()
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



function initialisation_substrate_concentration()
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% initialisation of the substrate concentration
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

global c b p g

% c cellulose matrix (at a givent time), memory preallocation and
% initialisation
c.concentration     = zeros(p.n,p.n);
c.concentration((p.gx.*p.gx+p.gy.*p.gy)<p.R*p.R) = c.ini;
%c.concentration( (p.gy<0.5*p.R)&(p.gy>-0.5*p.R) ) = c.ini;

end % of initialisation_substrate_concentration()
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



function initialisation_bacterial_population()
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% initialisation of the bacterial population
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

global c b p g

% bacteria array memory allocation
b.mass     = zeros(b.max_size,1); % mass of the bacteria
b.pos      = zeros(b.max_size,2); % position of the bacteria

switch p.ini_type
    
    case 'solo' %----------------------------------------------------------
        % 1 initial bacteria in the center
        
        b.mass(1)  = b.mass_ini;
        b.pos(1,1) = 0;
        b.pos(1,2) = 0;
        b.size_ini = 1 ;
    
    case 'uniform_border' %------------------------------------------------
        % initial bacterias will be uniformly distributed arround the
        % the initial substrate circle
                
        b.mass(1:b.size_ini)  = b.mass_ini;
        angle                 = 2*pi*rand(1,b.size_ini);
        b.pos(1:b.size_ini,1) = p.R*cos(angle);
        b.pos(1:b.size_ini,2) = p.R*sin(angle);
        
    case 'uniform' %-------------------------------------------------------
        % initial bacterias will be uniformly distributed outside the
        % the initial substrate circle

        i = 0;
        while i<b.size_ini
            xx = 2*p.L*rand()-p.L;
            yy = 2*p.L*rand()-p.L;
            if (xx*xx+yy*yy)>p.R*p.R
                i          = i+1;
                b.mass(i)  = b.mass_ini;
                b.pos(i,1) = xx;
                b.pos(i,2) = yy;
            end
        end
        
end

b.sizepop = b.size_ini;


end % of initialisation_bacterial_population()
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



function substrate_diffusion()
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% diffusion of the substrate
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

global c b p g

% STRUCTURE of the of cells (grid)
% the set of grid-cells can be decomposed as:
%
%                                        domain of the PDE
%                         --------------------------------------------
%     fix substrate       +       border            +         interior
% method from simple_substrat_2Da

% time increments dt_FD that respect the stability condition :
%      2 c.diff dt_FD / h^2 < 1/2
% so that dt_FD = 0.25 h^2/c.diff

domain = c.concentration<c.min;

t_old = 0;
t_new = 0;

while t_new < p.dt
    
    t_new = min( p.dt , t_old+p.dt_FD );
    ddt   = t_new - t_old;
    
    ip = circshift(domain,[-1  0]); % (i,j) cells with (i+1,j  ) in domain
    im = circshift(domain,[ 1  0]); % (i,j) cells with (i-1,j  ) in domain
    jp = circshift(domain,[ 0 -1]); % (i,j) cells with (i  ,j+1) in domain
    jm = circshift(domain,[ 0  1]); % (i,j) cells with (i  ,j-1) in domain
    
    neighb = im+ip+jm+jp; % number of free neighboor cells
    
    % --- finite difference scheme with Neumann boundary conditions
    snew = c.concentration + c.diff * ddt * p.hh2 * (  ...
          ip     .* circshift(c.concentration,[-1  0]) ...
        + im     .* circshift(c.concentration,[ 1  0]) ...
        + jp     .* circshift(c.concentration,[ 0 -1]) ...
        + jm     .* circshift(c.concentration,[ 0  1]) ...
        - neighb .* c.concentration);
    
    % --- substrate update
    c.concentration(domain) = snew(domain);
    
    t_old = t_new;
    
end

end % substrate_diffusion()
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



function bacteria_displacement()
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% displacement of the bacteria
%    the bacteria in s>c.min do not move
%    the bacteria in s<=c.min move in the gradient of the substrat + noise
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
global c b p g

% --- les gradients en chaque point de la grille
%     convolve2 is 10 times faster than conv2

ss = convolve2(c.concentration,b.ker_perc,'shape');
%ss = conv2(c.concentration,b.ker_perc,'shape');

vx  = b.mov_drift * p.hc2 * (circshift(ss,[-1  0]) - circshift(ss,[1 0]));
vy  = b.mov_drift * p.hc2 * (circshift(ss,[ 0 -1]) - circshift(ss,[0 1]));

vx = max(min(vx,b.speed_max),-b.speed_max);
vy = max(min(vy,b.speed_max),-b.speed_max);

% --- indices des pts de grille associ�s aux bact�ries

i_grid = ceil( (b.pos(1:b.sizepop,1)+p.L) / p.h );
j_grid = ceil( (b.pos(1:b.sizepop,2)+p.L) / p.h );
l_grid = sub2ind([p.n,p.n],i_grid,j_grid); % indice lineaire

% --- the bacteria in s>c.min do not move

i_bacteria = find(c.concentration(l_grid)<c.min);

if ~isempty(i_bacteria)

    % --- x1 coordinates
    b.pos(i_bacteria,1) = b.pos(i_bacteria,1) ...
        +                  p.dt      * vx(l_grid(i_bacteria)) ...
        + b.mov_diff_std * p.sqrt_dt * randn(length(i_bacteria),1);
    
    % --- x2 coordinates
    b.pos(i_bacteria,2) = b.pos(i_bacteria,2) ...
        +                  p.dt      * vy(l_grid(i_bacteria)) ...
        + b.mov_diff_std * p.sqrt_dt * randn(length(i_bacteria),1);
    
    % --- deplacement sur le tore [-L,L]^2
    b.pos(i_bacteria,1) = b.pos(i_bacteria,1) ...
        - 2 * p.L * floor((b.pos(i_bacteria,1)+p.L)/(2*p.L));
    b.pos(i_bacteria,2) = b.pos(i_bacteria,2) ...
        - 2 * p.L * floor((b.pos(i_bacteria,2)+p.L)/(2*p.L));
    
end

end % bacteria_displacement()
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



function  bacteria_consumption()
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% consumption of the bacteria
% A bacteria in position (x0,y0) wants to consume:
%           exp(b.ker_cons_c*((p.gx-x0).^2 + (p.gy-y0).^2))
% depending on the local avaibility of the cellulose
% Simplification : each bacteria eats one at a time (first arrived
% first served) + shuffle on the bacteria indices.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

global c b p g

% WARNING : this is sequential : first bacteria is served first
% se we shuffle the bacteria indices

indices = randperm(b.sizepop);

for i=1:b.sizepop
    
    ii = indices(i);
    x0 = b.pos(ii,2);
    y0 = b.pos(ii,1);
    
    % --- ressource that the bacteria wants ---------------------------
    
    % ressource uptake not on the torus (20 times faster)
    
    ker_aux = exp(b.ker_cons_c*((p.gx-x0).^2 + (p.gy-y0).^2));
    ker_aux = ker_aux / (sum(sum(ker_aux)) * p.h2);
    
    % the same on the torus (not so interesting)
    % cons_desired      = exp(-coef*((p.gx-x0    ).^2 + (p.gy-y0    ).^2))...
    %     +exp(b.ker_cons_c*((p.gx-x0-2*p.L).^2 + (p.gy-y0    ).^2))...
    %     +exp(b.ker_cons_c*((p.gx-x0+2*p.L).^2 + (p.gy-y0    ).^2))...
    %     +exp(b.ker_cons_c*((p.gx-x0    ).^2 + (p.gy-y0-2*p.L).^2))...
    %     +exp(b.ker_cons_c*((p.gx-x0    ).^2 + (p.gy-y0+2*p.L).^2))...
    %     +exp(b.ker_cons_c*((p.gx-x0+2*p.L).^2 + (p.gy-y0+2*p.L).^2))...
    %     +exp(b.ker_cons_c*((p.gx-x0+2*p.L).^2 + (p.gy-y0-2*p.L).^2))...
    %     +exp(b.ker_cons_c*((p.gx-x0-2*p.L).^2 + (p.gy-y0+2*p.L).^2))...
    %     +exp(b.ker_cons_c*((p.gx-x0-2*p.L).^2 + (p.gy-y0-2*p.L).^2));
    
    % on normalise pour "prendre" dt*b.cons_rate
    cons_desired = p.dt * b.cons_rate * ker_aux;
    
    % --- what it gets ------------------------------------------------
    
    cons_obtained   = min(c.concentration,cons_desired);
    c.concentration = c.concentration-cons_obtained;
    b.mass(ii)      = b.mass(ii) + sum(sum(cons_obtained)) * p.h2 * b.cons_yield;
    
end

end % bacteria_consumption()
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



function bacteria_duplication()
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% duplication of the bacteria
% if the mass of a bacteria > mass_dup, then it gives 2 bacteria with the
% same position and the half of the mass
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

global c b p g

% le vectoriel n'apporte pas grand chose ici

i_dup         = find(b.mass(1:b.sizepop)>b.mass_dup);
n_new_bac     = length(i_dup);

if (b.sizepop + n_new_bac) > b.max_size
    error([' ERROR maximum number of bactecria %i reached \n ' ...
        'raise this number !!!'],b.max_size);
end
    
    
b.mass(i_dup) = 0.5 * b.mass(i_dup);
b.mass((b.sizepop+1):(b.sizepop+n_new_bac))   = b.mass(i_dup);
b.pos( (b.sizepop+1):(b.sizepop+n_new_bac),1) = b.pos( i_dup,1);
b.pos( (b.sizepop+1):(b.sizepop+n_new_bac),2) = b.pos( i_dup,2);
b.sizepop = b.sizepop + n_new_bac;



end % bacteria_duplication()
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



function save_data()
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% save the current state
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

global c b p g

g.cputime_new = cputime;
g.i_out     = g.i_out+1;

g.cputime_old = g.cputime_new;

g.t_last_out     = p.t;

% --- saved instants

g.t_out(g.i_out) = g.t_last_out;

% --- spatial variables

c.concentration_out(:,:,g.i_out) = c.concentration;
b.pos_out(:,:,g.i_out)           = b.pos;
b.mass_out(:,:,g.i_out)          = b.mass;

% --- aggregate variables

c.total_out(g.i_out)             = sum(sum(c.concentration)) * p.h2;
b.mass_total_out(g.i_out)        = sum(b.mass(1:b.sizepop));
b.sizepop_out(g.i_out)           = b.sizepop;


end % of save_data()
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%






%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% graphic functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%




function graphics_interface()
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% plot a snapshot of the state of the system
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

global c b p g


% --- graphics ------------------------------------------------------------

p.fileID = 1;

while true
    
    fprintf(['    \n' ...
        '      >>> pick up a graphic or a video \n' ...
        '            1. temporal plot t -> population size and total biomass \n' ...
        '            2. temporal plot t -> total cellulose mass \n' ...
        '            3. full video\n' ...
        '            q. quit \n']);
    
    answer=input('            choice: ','s');
    
    
    switch answer
        
        case '1' % --- plot_pop_size_and_biomass ------------------------------
            
            clf
            
            g.i_out = g.i_out_last;
            plot_pop_size_and_biomass()
            
            f_pause_continue('   arrange the figure and press <return> ');
                      
            saveas(gcf,[p.work_dir '/graph_pop_size_and_biomass.fig'],'fig')
            vv = version;
            if vv(1) =='6'
                saveas(gcf,[p.work_dir '/graph_pop_size_and_biomass.eps'],'eps')
            else
                save2pdf([p.work_dir '/graph_pop_size_and_biomass.pdf'],gcf)
            end
            
        case '2' % --- plot_total_cellulose -------------------------------
            
            clf
            
            g.i_out = g.i_out_last;
            plot_total_cellulose()
            
            f_pause_continue('   arrange the figure and press <return> ');
                      
            saveas(gcf,[p.work_dir '/graph_total_cellulose.fig'],'fig')
            vv = version;
            if vv(1) =='6'
                saveas(gcf,[p.work_dir '/graph_total_cellulose.eps'],'eps')
            else
                save2pdf([p.work_dir '/graph_total_cellulose.pdf'],gcf)
            end

        case '3' % --- full_video -----------------------------------------
            
            clf
            make_the_movie
            
            
        otherwise % -------------------------------------------------------
            
            break
            
    end
    
    
end


end % of graphics_interface() 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



function graphics_interface_mc()
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% plot a snapshot of the state of the system
% MC version : same as graphics_interface() with extra features
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

global c b p g


% --- graphics ------------------------------------------------------------

p.fileID = 1;

while true
    
    fprintf(['    \n' ...
        '      >>> pick up a graphic or a video \n' ...
        '            1. temporal plot t -> population size and total biomass \n' ...
        '            2. temporal plot t -> total cellulose mass \n' ...
        '            3. full video\n' ...
        '            4. MC temporal plot t -> population size  \n' ...
        '            5. MC temporal plot t -> population biomass \n' ...
        '            6. MC temporal plot t -> total cellulose mass \n' ...
        '            q. quit \n']);
    
    answer=input('            choice: ','s');
    
    
    switch answer
        
        case '1' % --- plot_pop_size_and_biomass ----------------------------------
            
            clf
            
            g.i_out = g.i_out_last;
            plot_pop_size_and_biomass()
            
            f_pause_continue('   arrange the figure and press <return> ');
                      
            saveas(gcf,[p.work_dir '/graph_pop_size_and_biomass.fig'],'fig')
            vv = version;
            if vv(1) =='6'
                saveas(gcf,[p.work_dir '/graph_pop_size_and_biomass.eps'],'eps')
            else
                save2pdf([p.work_dir '/graph_pop_size_and_biomass.pdf'],gcf)
            end
            
        case '2' % --- plot_total_cellulose -------------------------------
            
            clf
            
            g.i_out = g.i_out_last;
            plot_total_cellulose()
            
            f_pause_continue('   arrange the figure and press <return> ');
                      
            saveas(gcf,[p.work_dir '/graph_total_cellulose.fig'],'fig')
            vv = version;
            if vv(1) =='6'
                saveas(gcf,[p.work_dir '/graph_total_cellulose.eps'],'eps')
            else
                save2pdf([p.work_dir '/graph_total_cellulose.pdf'],gcf)
            end

        case '3' % --- full_video -----------------------------------------
            
            clf
            make_the_movie
            
            
        case '4' % --- plot_pop_size_mc -------------------------------
            
            clf
            
            g.i_out = g.i_out_last;
            plot_pop_size_mc()
            
            f_pause_continue('   arrange the figure and press <return> ');
                      
            saveas(gcf,[p.work_dir '/graph_pop_size_mc.fig'],'fig')
            vv = version;
            if vv(1) =='6'
                saveas(gcf,[p.work_dir '/graph_pop_size_mc.eps'],'eps')
            else
                save2pdf([p.work_dir '/graph_pop_size_mc.pdf'],gcf)
            end
            
        case '5' % --- plot_pop_biomass_mc -------------------------------
            
            clf
            
            g.i_out = g.i_out_last;
            plot_pop_biomass_mc()
            
            f_pause_continue('   arrange the figure and press <return> ');
                      
            saveas(gcf,[p.work_dir '/graph_pop_biomass_mc.fig'],'fig')
            vv = version;
            if vv(1) =='6'
                saveas(gcf,[p.work_dir '/graph_pop_biomass_mc.eps'],'eps')
            else
                save2pdf([p.work_dir '/graph_pop_biomass_mc.pdf'],gcf)
            end
            
        case '6' % --- plot_total_cellulose_mc ----------------------------
            
            clf
            
            g.i_out = g.i_out_last;
            plot_total_cellulose_mc()
            
            f_pause_continue('   arrange the figure and press <return> ');
                      
            saveas(gcf,[p.work_dir '/graph_total_cellulose_mc.fig'],'fig')
            vv = version;
            if vv(1) =='6'
                saveas(gcf,[p.work_dir '/graph_total_cellulose_mc.eps'],'eps')
            else
                save2pdf([p.work_dir '/graph_total_cellulose_mc.pdf'],gcf)
            end
            
        otherwise % -------------------------------------------------------
            
            break
            
    end
    
    
end


end % of graphics_interface_mc() 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



function snapshot()
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% plot a snapshot of the state of the system
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

global c b p g

% --- some reshape

c.concentration = reshape(c.concentration_out(:,:,g.i_out),size(c.concentration));
b.pos(1:b.sizepop,1) = reshape(b.pos_out(1:b.sizepop,1,g.i_out),size(b.pos(1:b.sizepop,1)));
b.pos(1:b.sizepop,2) = reshape(b.pos_out(1:b.sizepop,2,g.i_out),size(b.pos(1:b.sizepop,2)));

clf

% --- four subplots

subplot(3,2,[1 3])
plot_cellulose_concentration_and_bacteria()
subplot(3,2,[2 4])
plot_cellulose_concentration()
subplot(3,2,5)
plot_total_cellulose()
subplot(3,2,6)
plot_pop_size_and_biomass()

drawnow
end % of snapShot()
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



function plot_cellulose_concentration_and_bacteria()
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% plot cellulose concentration and bacetria at time g.t_out(g.i_out)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

global c b p g

colormap(gray);
imagesc([-p.L,p.L],[-p.L,p.L],c.concentration,[0,c.ini]);
%imagesc([-p.L,p.L],[-p.L,p.L],reshape(c.concentration_out(:,:,g.i_out),size(c.concentration);
hold on
% ATTENTION scatter est beaucoup plus long que plot
%scatter(b.pos(1:b.sizepop,2),b.pos(1:b.sizepop,1),100*b.mass(1:b.sizepop),'r','filled');
plot(b.pos_out(1:b.sizepop,2,g.i_out),b.pos_out(1:b.sizepop,1,g.i_out),'o','MarkerSize',4,...
     'MarkerEdgeColor','r','MarkerFaceColor','r')
xlim([-p.L,p.L]);
ylim([-p.L,p.L]);
title('cellulose concentration & bacteria')
axis square;
freezeColors
axis off

end % of plot_cellulose_concentration_and_bacteria()
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



function plot_cellulose_concentration()
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% plot cellulose concentration at g.t_out(g.i_out)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

global c b p g

colormap(jet);
imagesc([-p.L,p.L],[-p.L,p.L],c.concentration,[0,c.ini]);
xlim([-p.L,p.L]);
ylim([-p.L,p.L]);
title('cellulose concentration')
axis square;
axis off

end % of plot_cellulose_concentration()
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



function plot_total_cellulose()
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% plot t -> total mass of cellulose for t = g.t_out(1:g.i_out)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

global c b p g

plot(g.t_out(1:g.i_out),c.total_out(1:g.i_out));
xlim([0 p.tmax]);
ylim([0 1.3*c.total_out(1)]);
xlabel('time (h)')
ylabel('total cellulose (pg)')

end % of plot_total_cellulose()
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



function plot_total_cellulose_mc()
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% plot t -> total mass of cellulose for t = g.t_out(1:g.i_out)
% MC vesion
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

global c b p g

mmean = mean(c.total_out_mc(:,1:g.i_out));
sstd  = sqrt((1/p.mc)*sum((c.total_out_mc(:,1:g.i_out)-repmat(mmean,p.mc,1)).^2));

plot(g.t_out(1:g.i_out),c.total_out_mc(:,1:g.i_out),'b');
hold on
plot(g.t_out(1:g.i_out),mmean,'r','LineWidth',2);
plot(g.t_out(1:g.i_out),mmean+2*sstd,'r--');
plot(g.t_out(1:g.i_out),mmean-2*sstd,'r--');
xlim([0 p.tmax]);
ylim([0 1.3*c.total_out(1)]);
xlabel('time (h)')
ylabel('total cellulose (pg)')

end % of plot_total_cellulose_mc()
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



function plot_pop_size_and_biomass()
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% plot t -> size of the population and total biomass 
% for t = g.t_out(1:g.i_out)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

global c b p g

[AX,H1,H2] = plotyy(g.t_out(1:g.i_out),b.sizepop_out(1:g.i_out),...
    g.t_out(1:g.i_out),b.mass_total_out(1:g.i_out),'plot','plot');%'semilogy','semilogy');
xlim(AX(1),[0 p.tmax])
ylim(AX(1),[0 100*ceil((2*c.total_out(1)*b.cons_yield/(b.mass_ini+b.mass_dup)+b.size_ini)/100)])
xlim(AX(2),[0 p.tmax])
ylim(AX(2),[0 100*ceil((c.total_out(1)*b.cons_yield+b.mass_total_out(1))/100)])
ylimits = get(AX(1),'YLim');
yinc = (ylimits(2)-ylimits(1))/4;
set(AX(1),'YTick',floor([ylimits(1):yinc:ylimits(2)]))
ylimits = get(AX(2),'YLim');
yinc = (ylimits(2)-ylimits(1))/4;
set(AX(2),'YTick',floor([ylimits(1):yinc:ylimits(2)]))    
set(get(AX(1),'Ylabel'),'String','population size') 
set(get(AX(2),'Ylabel'),'String','total biomass (pg)') 
xlabel('time (h)') 

end % of plot_pop_size_and_biomass()
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



% function plot_pop_size_and_biomass_mc()
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % plot t -> size of the population and total biomass 
% % for t = g.t_out(1:g.i_out)
% % MC version
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% global c b p g
% 
% [AX,H1,H2] = plotyy(g.t_out(1:g.i_out),b.sizepop_out_mc(:,1:g.i_out),...
%     g.t_out(1:g.i_out),b.mass_total_out_mc(:,1:g.i_out),'plot','plot');%'semilogy','semilogy');
% xlim(AX(1),[0 p.tmax])
% ylim(AX(1),[0 100*ceil((2*c.total_out(1)*b.cons_yield/(b.mass_ini+b.mass_dup)+b.size_ini)/100)])
% xlim(AX(2),[0 p.tmax])
% ylim(AX(2),[0 100*ceil((c.total_out(1)*b.cons_yield+b.mass_total_out(1))/100)])
% ylimits = get(AX(1),'YLim');
% yinc = (ylimits(2)-ylimits(1))/4;
% set(AX(1),'YTick',floor([ylimits(1):yinc:ylimits(2)]))
% ylimits = get(AX(2),'YLim');
% yinc = (ylimits(2)-ylimits(1))/4;
% set(AX(2),'YTick',floor([ylimits(1):yinc:ylimits(2)]))    
% set(get(AX(1),'Ylabel'),'String','population size') 
% set(get(AX(2),'Ylabel'),'String','total biomass (pg)') 
% xlabel('time (h)') 
% set(AX(1),'YColor','b') 
% set(AX(2),'YColor',[0 0.6 0]) 
% set(H1,'Color','b')
% set(H2,'Color',[0 0.6 0])
% 
% end % of plot_pop_size_and_biomass_mc()
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



function plot_pop_size_mc()
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% plot t -> total size of the population 
% for t = g.t_out(1:g.i_out)
% MC version
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

global c b p g

mmean = mean(b.sizepop_out_mc(:,1:g.i_out));
sstd  = sqrt((1/p.mc)*sum((b.sizepop_out_mc(:,1:g.i_out)-repmat(mmean,p.mc,1)).^2));

H1 = plot(g.t_out(1:g.i_out),b.sizepop_out_mc(:,1:g.i_out),'b');
hold on
plot(g.t_out(1:g.i_out),mmean,'r','LineWidth',2);
plot(g.t_out(1:g.i_out),mmean+2*sstd,'r--');
plot(g.t_out(1:g.i_out),mmean-2*sstd,'r--');
xlim([0 p.tmax])
ylim([0 100*ceil((2*c.total_out(1)*b.cons_yield/(b.mass_ini+b.mass_dup)+b.size_ini)/100)])
ylimits = get(gca,'YLim');
yinc = (ylimits(2)-ylimits(1))/4;
set(gca,'YTick',floor([ylimits(1):yinc:ylimits(2)]))
ylabel('population size') 
xlabel('time (h)') 

end % of plot_pop_size_mc()
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



function plot_pop_biomass_mc()
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% plot t -> total biomass of the population 
% for t = g.t_out(1:g.i_out)
% MC version
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

global c b p g

mmean = mean(b.mass_total_out_mc(:,1:g.i_out));
sstd  = sqrt((1/p.mc)*sum((b.mass_total_out_mc(:,1:g.i_out)-repmat(mmean,p.mc,1)).^2));

H2 = plot(g.t_out(1:g.i_out),b.mass_total_out_mc(:,1:g.i_out),'b');
hold on
plot(g.t_out(1:g.i_out),mmean,'r','LineWidth',2);
plot(g.t_out(1:g.i_out),mmean+2*sstd,'r--');
plot(g.t_out(1:g.i_out),mmean-2*sstd,'r--');
xlim([0 p.tmax])
ylim([0 100*ceil((c.total_out(1)*b.cons_yield+b.mass_total_out(1))/100)])
ylimits = get(gca,'YLim');
yinc = (ylimits(2)-ylimits(1))/4;
set(gca,'YTick',floor([ylimits(1):yinc:ylimits(2)]))    
ylabel('total biomass (pg)') 
xlabel('time (h)') 

end % of plot_pop_biomass_mc()
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



function make_the_movie()
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% simulation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

global c b p g

g.movie = 1;

clf
p.aviobj = avifile([p.work_dir '/cellulose_full_video.avi'],'compression','None');
p.fig    = figure('Position',[0 0 g.movie_window_size g.movie_window_size]);
% don't work .... input('      > adjust the movie layout [and <return>]','s');
g.i_out = 0;

while g.i_out < g.i_out_last
    g.i_out  = g.i_out+1;
    b.sizepop = b.sizepop_out(g.i_out);
    snapshot()
    F        = getframe(p.fig);
    p.aviobj = addframe(p.aviobj,F);
end

snapshot()         % final graphics
F        = getframe(p.fig);
p.aviobj = addframe(p.aviobj,F);

fprintf(p.fileID,'      \t saving the movie .... ');
close(p.fig);
p.aviobj = close(p.aviobj);
fprintf(p.fileID,'done \n');


end % of make_the_movie()
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



function f_pause_continue(chain)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% function f_pause_continue
% just pause and wait....(return or any other character...)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if isempty(chain)
    chain = '... pause [type return to continue]';
end
fprintf(['         ' chain]) ;
pause
fprintf('     \n') ;
end
%%% end of f_pause_continue %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

