function cellulose_run_all_tests()
%-------------------------------------------------------------------------------%
% main function, to launch mc series with various parameter values,
% to modify for each different run              %
%-------------------------------------------------------------------------------%
parallelwork                       = 1;
ParamRef                           = 'param_bact_size_ini';
ValuesRef                          = [5, 10, 20, 50, 100, 150, 200];
ParamOthers                        = {'param_cell_min';'param_cell_ini'};
ValuesOther                        = [[0.3],           [0.4]];

%if parallelwork                    == 1
%parallel computing does not work yet
%  matlabpool open 6
%end

cellulose_launch_several_mc(ParamRef, ValuesRef, ParamOthers, ValuesOther, parallelwork);
%if parallelwork                    == 1
%parallel computing does not work yet
%  matlabpool close
%end
end % of cellulose_run_all_tests


function cellulose_launch_1mc(param_bact,param_cell, param_par, param_graph, parallelwork, Param, ParamRef, value, ref)
%-------------------------------------------------------------------------------%
% to launch the mc trials on a given set of parameter values by calling
%  the script cellulose_mc_series            
%-------------------------------------------------------------------------------%
ResultFolder                       = cellulose_mc_series(3, param_bact, param_cell, param_par, param_graph);
dos(['copy cellulose_run_all_tests.m ' ResultFolder]);
%NewName                            = [ResultFolder(4:14) '_' Param(7:length(Param)) '-' num2str(value) '_' ParamRef(7:length(ParamRef)) '-' num2str(ref)];
%disp(pwd);
%disp(ResultFolder);
%disp(NewName);
%movefile([pwd '\' ResultFolder], [pwd '\' NewName])
%dos(rd nomrepertoire /S /Q)
%xcopy c:\mesdocs d: 
%dos(['MOVE ' ResultFolder ' ' NewName]);
end % of cellulose_launch_1mc


function cellulose_launch_several_mc(ParamRef, ValuesRef, ParamOthers, ValuesOther, parallelwork)
%-------------------------------------------------------------------------------%
% to launch all the required mc trials using for loops
%-------------------------------------------------------------------------------%
% default parameter values
param_par.n                          = 250;      % number of case in each direction (total n^2)
param_par.L                          = 40;       % 1/2 length of the domain domain=[-L,L]^2                (µm)
param_par.R                          = 25;       % radius of the subtrat circle                            (µm)
param_par.tmax                       = 150;      % final time                                              (h)
param_par.dt                         = 0.3;      % IBM time step (= EDP time step)                         (h)
param_cell.ini                       = 0.4;      % initial concentration in the substrate circle           (pg/µm^2)
param_cell.min                       = 0.3;      % diffusion threshold of substrate concentration          (pg/µm^2)
param_cell.diff                      = 5;        % diffusion parameter                                     (µm^2/h)
param_bact.size_ini                  = 50;       % initial bacterial population size                       (-)
param_bact.mass_ini                  = 0.4;      % initial mass of 1 bacteria                              (pg)
param_bact.mass_dup                  = 0.8;      % mass of duplication                                     (pg)
param_bact.speed_max                 = 10;       % max speed of the bacteria                               (µm/h)
param_bact.mov_drift                 = 0.1;      % drift coefficient                                       (µm^4/(pg h))
param_bact.mov_diff_std              = 1;        % sqrt(diffusion) coefficient                             (µm/sqrt(h))
param_bact.cons_yield                = 0.2;      % absorption percentage                                   (-)
param_bact.cons_rate                 = 0.2;      % consumption rate                                        (pg/h)
param_bact.ker_cons_std              = 1;        % standard deviation                                      (µm)
param_bact.ker_perc_std              = 2;        % standard deviation                                      (µm)
param_par.ini_type                   = 'uniform';% type of initial spacial distribution for the bacteria   (-)
param_graph.nb_out                   = 500;      % number of time snapshots                                (-)
param_graph.movie                    = 0;        % do a movie or not                                       (-)
param_graph.movie_window_size        = 500;      % size of the movie window                                (-)
param_par.iseed                      = 9999;     % 
param_par.mc                         = 500;        % number of MC trials                                     (-)

%ParamStruct is not used
%ParamStruct                         = struct('param_par_n', param_par.n, 'param_par_L', param_par.L, 'param_par_R', param_par.R, 'param_par_tmax', param_par.tmax, 'param_par_dt', param_par.dt, 'param_cell_ini', param_cell.ini, 'param_cell_min', param_cell.min, 'param_cell_diff', param_cell.diff, 'param_bact_size_ini', param_bact.size_ini, 'param_bact_mass_ini', param_bact.mass_ini, 'param_bact_mass_dup', param_bact.mass_dup, 'param_bact_speed_max', param_bact.speed_max, 'param_bact_mov_drift', param_bact.mov_drift, 'param_bact_mov_diff_std', param_bact.mov_diff_std, 'param_bact_cons_yield', param_bact.cons_yield, 'param_bact_cons_rate', param_bact.cons_rate, 'param_bact_ker_cons_std', param_bact.ker_cons_std, 'param_bact_ker_perc_std', param_bact.ker_perc_std, 'param_par_ini_type', param_par.ini_type);

if parallelwork                      == 1
% pour paralleliser, "dupliquer" les structures param ... autant de fois que necessaire dans un tableau
% puis parcourir le tableau, a faire + tard ...
  for indexParam                     = 1:length(ParamOthers)
    Param                            = char(ParamOthers(indexParam));
    for indexValue                   = 1:length(ValuesOther(indexParam))
      value                          = ValuesOther(indexValue);
      for indexRef                   = 1:length(ValuesRef)
        ref                          = ValuesRef(indexRef);
        param_bact_temp              = param_bact;
        param_cell_temp              = param_cell;
        param_graph_temp             = param_graph;
        param_par_temp               = param_par;
        if      Param(1:10)          == 'param_bact'
          disp('hello1');
          param_bact_temp.(Param(12:end))= value;
        elseif  Param(1:10)          == 'param_cell'
          disp('hello2');
          param_cell_temp.(Param(12:end))    = value;
        elseif  Param(1:11)          == 'param_graph'
          disp('hello3');
          param_graph_temp.(Param(13:end))   = value;
        elseif  Param(1:9)           == 'param_par'
          disp('hello4');
          param_par_temp.(Param(11:end))     = value;
        end
        if      ParamRef(1:10)       == 'param_bact'
          disp('hello5');
          param_bact_temp.(ParamRef(12:end)) = ref;
        elseif  ParamRef(1:10)       == 'param_cell'
          disp('hello6');
          param_cell_temp.(ParamRef(12:end)) = ref;
        elseif  ParamRef(1:11)       == 'param_graph'
          disp('hello7');
          param_graph_temp.(ParamRef(13:end))= ref;
        elseif  ParamRef(1:9)        == 'param_par'
          disp('hello8');
          param_par_temp.(ParamRef (11:end))  = ref;
        end
        disp(param_bact_temp);
        disp(param_cell_temp);
        cellulose_launch_1mc(param_bact_temp,param_cell_temp, param_par_temp, param_graph_temp, parallelwork, Param, ParamRef, value, ref);
      end
    end
  end
else
end

end % of cellulose_launch_several_mc
