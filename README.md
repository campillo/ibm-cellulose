# IBM cellulose

More details in this [page](http://www-sop.inria.fr/members/Fabien.Campillo/software/ibm-cellulose/index.html).

## Description
IBM simulation of the degradation of a cellulose bead (2D) 
by a colony of bacteria [celulose.pdf](cellulose.pdf).

## Visuals
See this [page](http://www-sop.inria.fr/members/Fabien.Campillo/software/ibm-cellulose/index.html).


## Usage

Under `matlab` you can run:

- `cellulose.m`
- `cellulose_mc.m`

## Authors and acknowledgment

For IBM cellulose we use:

- `convolve2.m` Fast 2-D convolution by David Young [[MATLAB Central File Exchange](https://www.mathworks.com/matlabcentral/fileexchange/22619-fast-2-d-convolution)]
- `save2pdf.m` of Gabe Hoffmann. Saves a figure as a properly cropped pdf
 [[MATLAB Central File Exchange](https://www.mathworks.com/matlabcentral/fileexchange/16179-save2pdf?s_tid=FX_rc2_behav)]
- `freezeColors.m` of John Iversen - Lock colors of plot, enabling multiple colormaps per figure. [[MATLAB Central File Exchange](https://se.mathworks.com/matlabcentral/fileexchange/7943-freezecolors-unfreezecolors)]


## License
[License](License.md)


## Project status
IBM cellulose is written with an ''old'' matlab, an update in a recent matlab would be welcome. 



## AUTHORS

IBM cellulose Authors: 

- [Fabien Campillo](http://www-sop.inria.fr/members/Fabien.Campillo/index.html), [MathNeuro Team](https://team.inria.fr/mathneuro/), [Inria centre
at Université Côte d’Azur](https://www.inria.fr/en/inria-centre-universite-cote-azur)
Fabien.Campillo@inria.fr
- [Ariane Bize](https://www6.jouy.inrae.fr/prose/L-unite/Pole-Porteurs-de-projets/Ariane-Bize), Inrae

With the help of [Chloé Deygout](https://www.linkedin.com/in/chloe-deygout/?originalSubdomain=fr).


